<?php
const HOST_PREFIX = 'todo';
const HOST_EXT = 'todo:8080';

function is_using_https() {
	return !empty($_SERVER['HTTPS']) and $_SERVER['HTTPS'] != 'off';
}
function requires_https() {
	if (!is_using_https()) {
		header('Location: https://' . $_SERVER['HTTP_HOST']);
		die('go to https://' . $_SERVER['HTTP_HOST']);
	}
}
function requires_http() {
	if (is_using_https()) {
		header('Location: http://' . $_SERVER['HTTP_HOST']);
		die('go to http://' . $_SERVER['HTTP_HOST']);
	}
}

function httponly() {
	echo '<h1>' . $_SERVER['HTTP_HOST'] . '</h1>';
	echo <<<'ENDINDEX'
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
<title>Beaver Peak Banking and BBQ</title>
<p>This domain belongs to a group of domains, aiming to demonstrate the goal of some SSL tools
during training.</p>
<p>The following page is shamelessly stolen from <a
href="https://blog.innerht.ml/google-yolo/">@filedescriptor</a> with slight modification, which
shamelessly stole from <a
href="https://lcamtuf.blogspot.com/2011/12/x-frame-options-or-solving-wrong.html">@lcamtuf</a> with
slight modification 😁</p>
<hr>
<img src="img/beaver-peak.jpg" style="float: left; margin-right: 10px">
<font size=+3 color=steelblue><b>Beaver Peak Banking and BBQ</b></font><br>
<i>"Best steaks in town!"</i>
-- Creek and Brook Daily
<br clear="all">
ENDINDEX;
	if (empty($_GET['page'])) {
		echo <<<'ENDINDEX'
<b>Welcome, please fill this authentication form!</b>

<form action='?page=login' method=POST>
	<label for=login>Login</label> <input id=login name=login><br>
	<label for=password>Password</label> <input id=password name=password type=password><br>
	<input type=submit>
</form>
ENDINDEX;
	} else if ($_GET['page'] == 'login') {
		echo <<<'ENDINDEX'
<p style='color: green; font-weight: bold'>Welcome, dear customer!</p>
<p style='color: green; font-weight: bold'>It's nice to see you again!</p>
<table style="border: 1px solid teal"><tr>
<td>Account number:</td><td>129834891</td></tr><tr>
<td>Account balance:</td><td>$5,392.00</td></tr></table><p>
ENDINDEX;
	}
	echo <<<'ENDINDEX'
<div style="border-width: 1px 0 0 0;border-color:steelblue; border-style:solid">
<font color=gray size=-1>Member FDIC. FDA certified. Truck parking available.</font>
</div>
ENDINDEX;
}


function sslstrip($subdomain) {
	echo '<h1>' . $_SERVER['HTTP_HOST'] . '</h1>';
	echo <<<'ENDINDEX'
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
<title>Beaver Peak Banking and BBQ</title>
<p>This domain belongs to a group of domains, aiming to demonstrate the goal of some SSL tools
during training.</p>
<p>The following page is shamelessly stolen from <a
href="https://blog.innerht.ml/google-yolo/">@filedescriptor</a> with slight modification, which
shamelessly stole from <a
href="https://lcamtuf.blogspot.com/2011/12/x-frame-options-or-solving-wrong.html">@lcamtuf</a> with
slight modification 😁</p>
<hr>
<img src="img/beaver-peak.jpg" style="float: left; margin-right: 10px">
<font size=+3 color=steelblue><b>Beaver Peak Banking and BBQ</b></font><br>
<i>"Best steaks in town!"</i>
-- Creek and Brook Daily
<br clear="all">
ENDINDEX;
	switch ($subdomain) {
		case '':
			echo <<<'ENDINDEX'
			<h2>News</h2>
			<p>Do you want to check some news?</p>
			<ul>
				<li><a href='//example.com'>A "beaver in the middle" attack was done to compromise John & Co.</a></li>
				<li><a href='//example.com'>New! BBQ goes vegan.</a></li>
				<li><a href='//example.com'>Be careful with buttons! Some attackers demonstrate some clickjacking vulns with our website!</a></li>
			</ul>
			<div style='margin: 10px 5%'>
			
			<a style='border:black solid 2px;' href="URL_ACCOUNT">Login</a></div>
ENDINDEX;
			break;
		case 'account':
			if (empty($_GET['page'])) {
				echo <<<'ENDINDEX'
<b>Welcome, please fill this authentication form!</b>

<form action='?page=login' method=POST>
	<label for=login>Login</label> <input id=login name=login><br>
	<label for=password>Password</label> <input id=password name=password type=password><br>
	<input type=submit>
</form>
ENDINDEX;
			} else if ($_GET['page'] == 'login') {
				echo <<<'ENDINDEX'
<p style='color: green; font-weight: bold'>Welcome, dear customer!</p>
<p style='color: green; font-weight: bold'>It's nice to see you again!</p>
<table style="border: 1px solid teal"><tr>
<td>Account number:</td><td>129834891</td></tr><tr>
<td>Account balance:</td><td>$5,392.00</td></tr></table><p>
ENDINDEX;
			}
			break;
	}
	echo <<<'ENDINDEX'
<div style="border-width: 1px 0 0 0;border-color:steelblue; border-style:solid">
<font color=gray size=-1>Member FDIC. FDA certified. Truck parking available.</font>
</div>
ENDINDEX;
}




$hostname = explode('.', $_SERVER['HTTP_HOST']);
$domain = implode('.', array_splice($hostname, -2));
$subdomain = implode('.', $hostname);

switch ($domain) {
	case HOST_PREFIX . '.' . HOST_EXT:
	case 'www.' . HOST_PREFIX . '.' . HOST_EXT:
		requires_https();
		echo '<h1>' . $_SERVER['HTTP_HOST'] . '</h1>';
		echo <<<'ENDINDEX'
<p>This group of domains aims to demonstrate the goal of some SSL tools during training.</p>
<p>The group of domains is composed of: </p>
<ul>
ENDINDEX;
		foreach([
			'http://' . HOST_PREFIX . '-httponly.' . HOST_EXT  => 
				'To demonstrate how dangerous is the lack of HTTPS',
			'http://' . HOST_PREFIX . '-sslstrip.' . HOST_EXT  =>
				'To demonstrate how dangerous is the lack of `Strict-Transport-Security`',
			'http://' . HOST_PREFIX . '-sslstrip2.' . HOST_EXT =>
				'To demonstrate how dangerous is the lack of `includeSubDomains`',
			'http://' . HOST_PREFIX . '-tofu.' . HOST_EXT =>
				// Will also use HOST_PREFIX . '-tofu-account.' . HOST_EXT
				'To demonstrate how dangerous is the lack of `preload`',
		] as $site => $descr) {
			echo '<li><a href="' . $site . '">' . $site . '</a>: ' . $descr . '</li>';
		}
		echo <<<'ENDINDEX'
</ul>
<p>The first page does not support HTTPS, in order to give to SSL interceptors a hook to strip the
`s` of HTTPS. Indeed, if all the navigation is secured with HTTPS, nobody will be able to intercept
the traffic.</p>
ENDINDEX;
		break;


	case HOST_PREFIX . '-httponly.' . HOST_EXT:
		requires_http();
		httponly();
		break;


	case HOST_PREFIX . '-sslstrip.' . HOST_EXT:
		if ($subdomain == 'account') {
			requires_https();
		} else {
			requires_http();
		}
		$a = function($buffer, $phase) {
			return str_replace('URL_ACCOUNT', 'https://account.' . $_SERVER['HTTP_HOST'], $buffer);
		};
		ob_start($a);
		sslstrip($subdomain);
		break;


	case HOST_PREFIX . '-sslstrip2.' . HOST_EXT:
		if ($subdomain == 'account') {
			// preload to avoid an interception due to the first request (TOFU model)
			header('Strict-Transport-Security: max-age=31536000; preload');
			requires_https();
		} else {
			requires_http();
		}
		$a = function($buffer, $phase) {
			return str_replace('URL_ACCOUNT', 'https://account.' . $_SERVER['HTTP_HOST'], $buffer);
		};
		ob_start($a);
		sslstrip($subdomain);
		break;


	case HOST_PREFIX . '-tofu.' . HOST_EXT:
		requires_http();
		$a = function($buffer, $phase) {
			return str_replace('URL_ACCOUNT', 'https://' . HOST_PREFIX . '-tofu-account.' . HOST_EXT, $buffer);
		};
		ob_start($a);
		sslstrip($subdomain);
		break;
	case HOST_PREFIX . '-tofu-account.' . HOST_EXT:
		requires_https();
		header('Strict-Transport-Security: max-age=31536000; includeSubDomains');
		sslstrip('account');
		break;


	default:
		if (empty(@$_SERVER['SERVER_ADMIN'])) {
			echo 'THE ADMIN FAILED THE CONFIGURATION. GO DISTURB HIM.';
		} else {
			echo 'THE ADMIN FAILED THE CONFIGURATION. GO DISTURB HIM BY SENDING AN EMAIL TO &lt;' .
				$_SERVER['SERVER_ADMIN'] . '&gt;.';
		}
}
?>
<hr>
<p><small>From <a href="https://gitlab.com/ajabep/ssltester">https://gitlab.com/ajabep/ssltester</a>. If you spotted a typo, please open an issue 😁</small></p>
