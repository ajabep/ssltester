SSLTester
=========

The goal of this project is to have a way to demonstrate the usage of SSLStrip, SSLStrip 2 and of
some other tools to attack SSL.

Indeed, because of the global improvement of SSL/TLS security, it's dificult to demontrate the usage
of these tools during some trainings.


FAQ
---

**Q**: Oh! All code in 1 file only?

**A**: Yep, the goal was to deploy it quickly, all in a file, for all domains.

**Q**: What are domains that I need to register to deploy it?

**A**: It's decribe in the source code. I do not make a list, in order to reduce the maintainance
noise. If you want to know all these domains without inspecting the source code, deploy it in a lab
(I mean with `php -S` and `vim /etc/hosts` 😁)

**Q**: OMG, THIS CODE IS SOOO UGLY!

**A**: Probably, it's a quick'n'dirty project. If you want to clean it, I accept pull requests.

**Q**: OMG, THIS SITE IS SOOO UGLY!

**A**: Yep, I'm not a webdesigner. If you want to improve it, I accept pull requests.

**Q**: PHP? Seriously? For a security projet?

**A**: Yep, it's a quick'n'dirty project. The goal was to dev this quickly and find quickly a hoster
or plug in an existant hosting solution, in order to reduce the cost of this demo project.

**Q**: I spoted a vulnerability! You have not deploy HTTPS correctly!

**A**: Read this file once again. If, after this, you continue to think that it is an error, open an
issue.
